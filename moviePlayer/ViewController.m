//
//  ViewController.m
//  moviePlayer
//
//  Created by Jumpei Kondo on 2012/11/02.
//  Copyright (c) 2012年 Jumpei.Kondo. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

-(MPMoviePlayerController*)makeMoviePlayer:(NSString*)res{
    //リソースのURL生成
//    NSString* path = [[NSBundle mainBundle] pathForResource:res ofType:@""];
//    NSURL* url = [NSURL fileURLWithPath:[NSBundle mainBundle] path];
    NSURL* url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Introducing_the_Leap" ofType:@"mp4"]];
    
    //ムービープレイヤーの生成
     player = [[MPMoviePlayerController alloc] initWithContentURL:url];
    player.scalingMode = MPMovieScalingModeAspectFit;
    player.controlStyle = MPMovieControlStyleEmbedded;
    return player;
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    //ムービープレイヤーの再生
    player = [self makeMoviePlayer:@"Introducing_the_Leap.mp4"];
    [player.view setFrame:CGRectMake(0, 0, 320, 320)];
    [self.view addSubview:player.view];
    
    //ムービー完了の通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    
    //ムービー再生
    [player play];
    
    //端末のボリューム操作
    MPVolumeView* volumeView = [MPVolumeView new];
    [volumeView setFrame:CGRectMake(0, 420, 320, 40)];
    [self.view addSubview:volumeView];
    
    
    //
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//ムービー完了時に呼ばれる
- (void)moviePlayBackDidFinish:(NSNotification*)notification{
    NSDictionary* userInfo = [notification userInfo];
    int reason = [[userInfo objectForKey:@"MPMoviePlayerPlaybackDidFinishReasonUserInfoKey"] intValue];
    if (reason == MPMovieFinishReasonPlaybackEnded) {
        NSLog(@"再生終了");
    }else if(reason == MPMovieFinishReasonPlaybackError){
        NSLog(@"エラー");
    }else if (reason == MPMovieFinishReasonUserExited){
        NSLog(@"フルスクリーン用UIのDoneボタンで終了");
    }
}
@end
