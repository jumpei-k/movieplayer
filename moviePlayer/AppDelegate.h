//
//  AppDelegate.h
//  moviePlayer
//
//  Created by Jumpei Kondo on 2012/11/02.
//  Copyright (c) 2012年 Jumpei.Kondo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ViewController *viewController;

@end
