# moviewPlayer #
A sample project for MediaPlayer API

## Create a media player object ##
```objective-C
-(MPMoviePlayerController*)makeMoviePlayer:(NSString*)res{

    NSURL* url = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"Introducing_the_Leap" ofType:@"mp4"]];
    
    player = [[MPMoviePlayerController alloc] initWithContentURL:url];
    player.scalingMode = MPMovieScalingModeAspectFit;
    player.controlStyle = MPMovieControlStyleEmbedded;
    return player;
    
}
```

## play movie ##
```objective-C
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    //ムービープレイヤーの再生
    player = [self makeMoviePlayer:@"Introducing_the_Leap.mp4"];
    [player.view setFrame:CGRectMake(0, 0, 320, 320)];
    [self.view addSubview:player.view];
    
    //ムービー完了の通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    
    //ムービー再生
    [player play];
    
    //端末のボリューム操作
    MPVolumeView* volumeView = [MPVolumeView new];
    [volumeView setFrame:CGRectMake(0, 420, 320, 40)];
    [self.view addSubview:volumeView];
    
}
```

## Notification ##
```objective-C
- (void)moviePlayBackDidFinish:(NSNotification*)notification{
    NSDictionary* userInfo = [notification userInfo];
    int reason = [[userInfo objectForKey:@"MPMoviePlayerPlaybackDidFinishReasonUserInfoKey"] intValue];
    if (reason == MPMovieFinishReasonPlaybackEnded) {
        NSLog(@"再生終了");
    }else if(reason == MPMovieFinishReasonPlaybackError){
        NSLog(@"エラー");
    }else if (reason == MPMovieFinishReasonUserExited){
        NSLog(@"フルスクリーン用UIのDoneボタンで終了");
}
```